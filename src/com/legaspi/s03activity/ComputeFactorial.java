package com.legaspi.s03activity;

import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String args[]) {

        System.out.println("Factorial Program Initiated.");
        int num = 0;
        try {

            Scanner input = new Scanner(System.in);

            System.out.println("Input any number to calculate the factorial: ");
            num = input.nextInt();
            long result = 1;
            for (var i = num; i > 0; i--) {
                result = result * i;
            }
            if (num < 0) {
                throw new Exception();
            }
            else {
                System.out.println("The factorial is: " + result);
            }

        } catch (Exception err) {
            if (num < 0) {
                System.out.println("The input should not be less than 0.");
            } else {
                System.out.println("Input invalid. Please provide a number.");
            }
        }
    }
}
